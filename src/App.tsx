import React from 'react';
import './App.css';
import InputField from './components/InputField';
import { useState } from 'react';
import { useEffect } from 'react';
import { Todo } from './components/Model';
import TodoList from './components/TodoList';

const App: React.FC = () => {

  const [todo, setTodo] = useState<string>("");
  const [todos, setTodos] = useState<Todo[]>([]);

  // console.log(todo);
  // console.log(todos)

  const handleAdd = (e: React.FormEvent<HTMLFormElement>) => {
    e.preventDefault();
    if(todo){
      setTodos(
          [
            ...todos,
            {
              id: Date.now(), 
              todo, 
              isDone: false
            }
          ]
      );
    }
  }

  return (
    <div className="App">
      <span className={'heading'}>Taskify</span>
      <InputField todo={todo} setTodo={setTodo} handelAdd={handleAdd}/>
      <TodoList todos={todos} setTodos={setTodos}/>
    </div>
  );
}

export default App;
