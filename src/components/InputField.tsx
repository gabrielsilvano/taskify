import React from 'react'
import './style.css'
import { useRef } from 'react';

export interface PropsTask {
    todo: string;
    setTodo:React.Dispatch<React.SetStateAction<string>>;
    handelAdd:(e: React.FormEvent<HTMLFormElement>) => void;
}

const InputField: React.FC<PropsTask> = ({todo, setTodo, handelAdd} : PropsTask ) => {
    const inputRef = useRef<HTMLInputElement>(null);

    return (
        <form className={'input'} 
        onSubmit={(e) => {
            handelAdd(e);
            setTodo('')
            inputRef.current?.blur();
        }}>
            <input 
            value={todo}
            onChange = {
                (e) => setTodo(e.target.value)
            }
            
            type={'input'}
            className={'input__box'} 
            placeholder={'Enter a Task'}/>
            <button className={'input_submit'} >GO</button>
        </form>
    )
}

export default InputField;
