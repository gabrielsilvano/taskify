import React from 'react';
import CardsTodo from './CardsTodo';
import { Todo } from './Model';
import './style.css'

interface Props{
    todos: Todo[];
    setTodos: React.Dispatch<React.SetStateAction<Todo[]>>
}

const TodoList: React.FC<Props> = ({todos, setTodos} : Props ) => {
    console.log(todos)
    return (
        <div className={'todos'}>
            {
                todos.map((todo) => (
                    <CardsTodo 
                    task={todo} 
                    tasks={todos}
                    setTasks={setTodos}
                    />
                ))
            }
        </div>
    )
}

export default TodoList;
