import React from 'react'
import { Todo } from './Model';
import { AiFillDelete, AiFillEdit } from 'react-icons/ai';
import { MdDone} from 'react-icons/md';
import './style.css'
import {useState, useRef, useEffect} from 'react'

type CardProps = {
    task: Todo;
    tasks: Todo[];
    setTasks: React.Dispatch<React.SetStateAction<Todo[]>>;
}

const CardsTodo: React.FC<CardProps> = ({task, tasks, setTasks}:CardProps ) => {
    const [edit, setEdit] = useState<boolean>(false)
    const [editTask, setEditTask] = useState<string>(task.todo)

    const handelDone = (id:number) =>{
        setTasks(
            tasks.map( 
                (todo) => 
                todo.id === id?
                {
                    ...todo, 
                    isDone: !todo.isDone
                }
                :todo)

        )
    }

    const handelDelete = (id:number) =>{
        setTasks(tasks.filter((task) => task.id !== id))
    }

    const handelEdit = (e : React.FormEvent, id: number ) => {
        e.preventDefault();
        setTasks(
            tasks.map((task) => (
                task.id === id? {
                    ...task,
                    todo: editTask
                }: task
            ))
        );
        setEdit(false);
    }

    useEffect(() => {
        inputRef.current?.focus();
    }, [edit])
    const inputRef = useRef<HTMLInputElement>(null)
    return (
        <form className={'todos_cards'} onSubmit={(e) => handelEdit(e,task.id)}>
            {
                edit? (
                    <input className={'todos_cards-text'} value={editTask} 
                    ref={inputRef}
                    onChange={
                        (e) => setEditTask(e.target.value)
                    }
                    />
                ):task.isDone ? (
                    <s className={'todos_cards-text'}>{task.todo}</s>
                ):(
                    <span className={'todos_cards-text'}>{task.todo}</span>
                )
            }
            
            
            
            <div>
                <span className='icons' onClick={() =>
                    {
                        if(!edit && !task.isDone){
                            setEdit(!edit);
                        }
                    }
                }>
                    <AiFillEdit/>
                </span>
                <span className='icons' onClick={() => handelDelete(task.id)}>
                    <AiFillDelete/>
                </span>
                <span className='icons' onClick={() => handelDone(task.id)}>
                    <MdDone/>
                </span>
            </div>
        </form>
    )
}

export default CardsTodo
